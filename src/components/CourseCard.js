// import {useState, useEffect} from 'react';
import {Container, Row, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom'


export default function CourseCard({courseProp}){
	// console.log(courseProp);
	// console.log(typeof courseProp);


	// // Object destructuring
	const {name, description, price, _id} = courseProp
	// // console.log(name);
	
	// //Syntax: const [getter, setter] = useState(initialValueofGetter)
	// const [count, setCount] = useState(0)
	// const [seats, setSeats] = useState(30)
	// const [isOpen, setIsOpen] = useState(false)

	// const enroll = () =>{
	// 	if (seats > 0) {
	// 		setCount(count + 1);
	// 		console.log('Enrollees: ' + count);

	// 		setSeats(seats - 1);
	// 		console.log('Seats: ' + seats);
	// 	} else {
	// 		alert("Last seat taken");
	// 		setIsOpen(true);
	// 	};
	// }
	// useEffect(() =>{
	// 	if(seats === 0){
	// 		alert("Last seat taken");
	// 		setIsOpen(true)
	// 	}
	// }, [seats])

	

	return(
		<Container>
		<Row className="mb-4">
			<Card className="cardHighlight p-3">
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>PHP {price}</Card.Text>
					<Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>
				</Card.Body>
			</Card>	
		</Row>
		</Container>
	)
}