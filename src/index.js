/*
Initial Setup for React.js...

1. npm install -g create-react-app
2. npx create-react-app booking-app (check if can do this step)
3. cd booking-app
4. npm start
5. Delete index.css, App.test, Logo and ReportWebVitals

6. At index.js... delete above items
7. npm i bootstrap react-bootstrap
8. Import Bootstrap, in index.js, add: import 'bootstrap/dist/css/bootstrap.min.css'
9. In src folder, create "components" folder and create AppNavBar.js file

Documentations: 
----- https://react-bootstrap.netlify.app/components/navbar/#navbars
----- https://react-bootstrap.github.io/

------------- React Router ------------------------------------
1. npm i react-router-dom
2. npm start
3. in App.js import router: import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

Documentation: https://reactrouter.com/docs/en/v6/getting-started/overview


*/


import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css'


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);



/*const name = 'John Smith';

const user = {
  firstName: 'Jane',
  lastName: 'Smith'
}

const formatName = (user) =>{
  return `${user.firstName} ${user.lastName}`
}
const element = <h1>Hello, {formatName(user)} </h1>

const root = ReactDOM.createRoot(document.getElementById('root'))

root.render(
  element
)*/