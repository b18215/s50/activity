import { useState, useEffect, useContext } from 'react';
import {Navigate} from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'


export default function Register(){

	const {user, setUser} = useContext(UserContext)
	

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password);

	useEffect(() => {
		if((firstName !== '' && lastName !== '' && mobileNo.length === 11 && email !== '' && password !== '')){

			setIsActive(true);
		
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, mobileNo, email, password]);

	function registerUser(e) {

		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmailExists',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data)
			//------------------------Swal Portion [START]-----------------------------
			if(data){
			Swal.fire({
				title: 'Registration Failed',
				icon: 'info',
				text: `${email} is already registered`	
			})
			} else {

				fetch('http://localhost:4000/users',{
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password
					})
				})
				.then(res => res.json())
				.then(data =>{
					console.log(data)
					if(data.email){
							Swal.fire({
							title: 'Registration complete.',
							icon: 'success',
							text: `${email} registration successful!`	
							})

							// history("/login")
					} else {
							Swal.fire({
								title: 'Registration complete.',
								icon: 'error',
								text: 'Something went wrong'	
						})
					}
			})
		}
	
			//------------------------Swal Portion [END]-----------------------------

		})

		setFirstName('');
		setLastName('');
		setMobileNo('');
		setEmail('');
		setPassword('');


	}

	
	return (
		(user.id !== null) ?
		<Navigate to= "/courses"/>
		:
		<>
		<h1>Register Here:</h1>
		<Form onSubmit={e => registerUser(e)}>

			<Form.Group controlId="firstName">
				<Form.Label>First Name:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Please input your first name here"
					required
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label>Last Name:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Please input your last name here"
					required
					value={lastName}
					onChange={e => setLastName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="mobileNo">
				<Form.Label>Mobile No.:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your 11-digit mobile number here"
					required
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="userEmail">
				<Form.Label>Email Address:</Form.Label>
				<Form.Control
					type="email"
					placeholder="Please input your email here"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Please input your password here"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>


  		 {
          <Button
            variant={isActive ? 'success' : 'danger'}
            type='submit'
            id='submitBtn'
            className='mt-3 mb-3'
            disabled={isActive ? false : true}
          >
            Submit
          </Button>
        }
		</Form>
		</>



	)
}
