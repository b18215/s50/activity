import {Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Error(){

	

	return(
		<Row>
			<Col className = "p-5">
				<h1>Page Not Found</h1>
				<Button as={Link} to="/" variant="primary">Return to homepage</Button>
			</Col>
		</Row>


	)
}